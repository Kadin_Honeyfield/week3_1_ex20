﻿using System;

namespace week3_1_ex20
{
    class Program
    {
        static void Main(string[] args) 
        {
            /**********************************
                Week 3 - Ex 20 - A)
            ***********************************/
/*
            //declaring variables 
            int num1;
            int num2;

            Console.WriteLine("Please enter two numbers and I will see if they're the same");
            Console.WriteLine("Please enter your first number");
            num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Now please enter another number to compare");
            num2 = int.Parse(Console.ReadLine());

            if (num1 == num2)
            
                Console.WriteLine("These are the same numbers!");
            
            else
            
                Console.WriteLine("These are not the same numbers!");
*/
            /***********************************
                Week 3 - Ex 20 - A)
            **********************************/

            /**********************************
                Week 3 - Ex 20 - B)
            **********************************/
/*
            //Declaring variables
            var password = "";
            var passwordcheck = "";

            Console.WriteLine("Please enter a password that is at least 8 characters long.");
            passwordcheck = Console.ReadLine();

            if (passwordcheck.Length > 8)
            {
                Console.WriteLine("Password set.");
                password = passwordcheck;
                Console.WriteLine($"Your password is {password}.");
            }
            else
            {
                Console.WriteLine("Your password isn't long enough.");
                Console.WriteLine("Try again.");
            }
*/
            /**********************************
                Week 3 - Ex 20 - B)
            **********************************/
            
            /**********************************
                Week 3 - Ex 20 - C)
            **********************************/

            //Declaring variables
            var password = "";
            var passwordCheck = "";

            Console.WriteLine("Please enter a password that is at least 8 characters long.");
            passwordCheck = Console.ReadLine();

            //Check if password is greater than 8 characters
            if (passwordCheck.Length > 8)
            {
                //If it is set password
                Console.WriteLine("Password set.");
                password = passwordCheck;
                Console.WriteLine($"Your password is {password}.");
            }
            else
            {
                //If not end application
                Console.WriteLine("Your password isn't long enough.");
                Console.WriteLine("Try again.");
                return;
            }

            //Changing the password using .Replace
            Console.WriteLine("-----------------------");
            Console.WriteLine("Changing your password.");
            Console.WriteLine("Enter in your new password");
            passwordCheck = Console.ReadLine();


            //Check if new password is greater than 8 characters
            if (passwordCheck.Length > 8)
            {
                //If it is change password
                Console.WriteLine("-----------------------");
                Console.WriteLine($"Your old password was '{password}'");
                Console.WriteLine($"You are currently changing it to '{passwordCheck}'");
                password = password.Replace(password, passwordCheck);
                Console.WriteLine("Complete.");
                Console.WriteLine($"Your new password is '{password}'");
            }
            else
            {
                //If not end application
                Console.WriteLine("-----------------------");
                Console.WriteLine("Your password isn't long enough.");
                Console.WriteLine("Try again.");
                return;
            }

            /**********************************
                Week 3 - Ex 20 - C)
            **********************************/          

        } 
    }
}
